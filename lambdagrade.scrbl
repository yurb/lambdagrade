#lang scribble/manual

@(require (for-label racket/base
                     racket/generator
                     json
                     lambdagrade/mediawiki
                     lambdagrade/struct
                     lambdagrade/contribution))

@title{λ grade}

λ grade is a jury tool for Wikipedia article contests, written in Racket.

@section{Internal APIs}

@subsection{MediaWiki}

@defmodule[lambdagrade/mediawiki]

@defparam[mediawiki-api-endpoint url string? #:value #f]{
 A url of the endpoint to query, eg. @racket["https://en.wikipedia.org/w/api.php"].
}

@defproc[(mw-request [params (listof (cons/c symbol? string?))]
                     [narrow procedure? #f])
         generator?]{

 Send a GET request to the MediaWiki API endpoint with query string
 set to the key-value pairs in @racket[params], and return a
 @racketlink[generator?]{generator} that will
 @racketlink[yield]{yield} results from the api. If @racket[narrow] is
 not specified, each yield corresponds to one response from MediaWiki,
 as a @racketlink[jsexpr?]{jsexpr}. If MediaWiki signals there is more
 data available, subsequent calls to the generator will yield
 subsequent portions of the data, as returned by the API. If no more
 data is available, the generator yields @racket[#f].

 If @racket[params] does not contain @racket['action], it defaults to
 @racket["query"].

 If @racket[narrow] is a procedure, it is called before each yield;
 the procedure is provided with the response (again, as a
 @racketlink[jsexpr?]{jsexpr}) and must return a list of values. Each
 of the values will be yielded separately before continuing to make
 the next request (if there is more data). This allows processing the
 response on a row-by-row basis instead of response-by-response.

 If MediaWiki responds with an error (i.e. the response contains
 @racket['errors] key),
 @racket[exn:fail:mediawiki] exception is
 raised with the response in the message.

}

@subsection{Calculating contributions}

@defmodule[lambdagrade/contribution]

@defproc[(fetch-article-revisions [contest contest?]
                                  [pageid exact-integer?])
         (values (listof hash?) (>=/c 0))]{
  Fetch a list of revisions of @racket[pageid] within contest timespan
  of @racket[context] and return two values:

  @itemlist[
    @item{list of revisions, if any, or empty}
    @item{size of the revision preceding the first revision within
          contest time span, if any, or 0}]
}

@defproc[(revisions-calculate-deltas [revisions (listof hash?)]
                                     [initial exact-nonnegative-integer?])
         (listof hash?)]{
  Given a list of article revisions, add a @racket['size-delta]
  key to each revision, where the delta is the difference in size
  from the previous revision. Return the new list.

  For the first revision, compare size to @racket[initial].
}

@defproc[(revisions->contributions [revisions (listof hash?)]
                                   [pageid exact-positive-integer?]
                                   [initial exact-nonnegative-integer?])
         (listof contribution?)]{
  For a list of revisions return a list of @racket[contribution?]
  each describing the contribution of a single editor to the article
  identified by @racket[pageid].

  @racket[initial] is the size of the article before the first contest
  edit (0 if article is new or was empty).
}
